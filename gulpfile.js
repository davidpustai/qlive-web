var gulp = require('gulp'),
	gulpLoadPlugins = require('gulp-load-plugins'),
	runSequence = require('run-sequence'),
	mergeStreams = require('merge-stream'),
	del = require('del'),
	$ = gulpLoadPlugins(),
	fs = require('fs'),
	_ = require('underscore');

// ===============================================================
// PLUGIN SETTINGS SHARED ACCROSS MULTIPLE TASKS
// ===============================================================
var revManifestsBase = '.tmp/rev-manifest';

// ===============================================================
// ENVIROMENT VARIABLES
// ===============================================================
var ENV = 'dist',
	DEST = 'dist';

// ===============================================================
// CLEAN
// ===============================================================
// Empty directories to start fresh.
gulp.task('clean', function() {
	return del(['.tmp', 'dev', 'dist']);
});

// ===============================================================
// SCSS -> CSS
// ===============================================================
gulp.task('sass', function() {
	return gulp.src('assets/scss/*.scss')
		.pipe($.sass({
			includePaths: ['assets/scss', 'bower_components'],
			precision: 6
		}).on('error', $.sass.logError))
		.pipe(gulp.dest('.tmp/css/compiled'));
});

// ===============================================================
// CONCAT CSS
// ===============================================================
// Source should be compiled CSS (.tmp/css/copmiled), resp. vendor CSS (bower_components resp. assets/vendor/css).
gulp.task('concat:css', ['sass'], function() {
	return gulp.src([
			'bower_components/normalize.css/normalize.css',
			'.tmp/css/compiled/main.css'
		])
		.pipe($.concat('main.css'))
		.pipe(gulp.dest('.tmp/css/concated'));
});

// ===============================================================
// CSS PROCESSING
// ===============================================================
gulp.task('css', ['concat:css'], function() {
	var stream = gulp.src('.tmp/css/concated/**/*.css')
		.pipe($.autoprefixer());

	if ( ENV == 'dist' ) {
		return stream
			.pipe($.combineMq())
			.pipe($.cleanCss())
			.pipe($.rev())
			.pipe(gulp.dest(DEST + '/assets/css'))
			.pipe($.rev.manifest(revManifestsBase + '/css.json', {
				base: revManifestsBase,
				merge: true
			}))
			.pipe(gulp.dest(revManifestsBase));
	}
	else {
		return stream
			.pipe(gulp.dest(DEST + '/assets/css'))
			.pipe($.connect.reload());
	}
});

// ===============================================================
// CONCAT JS
// ===============================================================
gulp.task('concat:js', [
	'concat:js:fonts',
	'concat:js:main',
	'concat:js:picturefill'
]);

gulp.task('concat:js:fonts', function() {
	return gulp.src([
			'bower_components/fontfaceobserver/fontfaceobserver.js',
			'assets/js/fonts.js'
		])
		.pipe($.concat('fonts.js'))
		.pipe(gulp.dest('.tmp/js/concated'));
});

gulp.task('concat:js:main', function() {
	return gulp.src([
			'assets/js/plugins.js',
			'bower_components/fastclick/lib/fastclick.js',
			'bower_components/frontender-toolbox/get_media_query/get_media_query.js',
			'assets/js/components/a11ySkipTo.js',
			'assets/js/components/mainNav.js',
			'assets/js/components/pageHeader.js',
			'assets/js/components/contactForm.js',
			'assets/js/main.js'
		])
		.pipe($.concat('main.js'))
		.pipe(gulp.dest('.tmp/js/concated'));
});

gulp.task('concat:js:picturefill', function() {
	return gulp.src([
			'bower_components/picturefill/dist/picturefill.min.js'
		])
		.pipe($.concat('picturefill.js'))
		.pipe(gulp.dest('.tmp/js/concated/vendor'));
});

// ===============================================================
// JS PROCESSING
// ===============================================================
gulp.task('js', ['concat:js'], function() {
	var stream = gulp.src('.tmp/js/concated/**/*.js');

	if ( ENV == 'dist' ) {
		return stream
			.pipe($.uglify())
			.pipe($.rev())
			.pipe(gulp.dest(DEST + '/assets/js'))
			.pipe($.rev.manifest(revManifestsBase + '/js.json', {
				base: revManifestsBase,
				merge: true
			}))
			.pipe(gulp.dest(revManifestsBase));
	}
	else {
		return stream
			.pipe(gulp.dest(DEST + '/assets/js'))
			.pipe($.connect.reload());
	}
});

// ===============================================================
// IMAGE PROCESSING
// ===============================================================
gulp.task('img', function() {
	return gulp.src([
			'assets/img/**/*.{gif,jpg,png,svg}',
			'!assets/img/favicons/**/*'
		])
		.pipe($.if(ENV == 'dist', $.imagemin()))
		.pipe(gulp.dest(DEST + '/assets/img'))
		.pipe($.connect.reload());
});

// ===============================================================
// COPY FILES
// ===============================================================
gulp.task('copy', [
	'copy:misc',
	'copy:icons'
]);

gulp.task('copy:misc', function() {
	return gulp.src([
			'.htaccess',
			'manifest.json',
			'browserconfig.xml',
			'scripts/**/*.php',
			'assets/font/**/*.{woff,woff2}',
			'!assets/font/original/**/*',
			'assets/js/vendor/jquery-3.1.1.js'
		], {
			base: '.'
		})
		.pipe(gulp.dest(DEST))
		.pipe($.connect.reload());
});

gulp.task('copy:icons', function() {
	return gulp.src([
			'assets/img/favicons/**/*.{ico,png,svg}'
		])
		.pipe(gulp.dest(DEST))
		.pipe($.connect.reload());
});

// ===============================================================
// RENDER HTML PAGES
// ===============================================================
// Run after SVGs are minified for inlining.
gulp.task('render_pages', ['img'], function() {
	var streams = mergeStreams(),
		global_data = {
			site_url: "https://qlive.cz/",
			current_year: new Date().getFullYear(),
			svg: {
				levels: {
					automatizace: fs.readFileSync(DEST+'/assets/img/levels/automatizace.svg'),
					osvetleni: fs.readFileSync(DEST+'/assets/img/levels/osvetleni.svg'),
					ozvuceni: fs.readFileSync(DEST+'/assets/img/levels/ozvuceni.svg'),
					zabezpeceni: fs.readFileSync(DEST+'/assets/img/levels/zabezpeceni.svg'),
					klimatizace: fs.readFileSync(DEST+'/assets/img/levels/klimatizace.svg'),
					interiery: fs.readFileSync(DEST+'/assets/img/levels/interiery.svg')
				},
				services: {
					experience: fs.readFileSync(DEST+'/assets/img/services/experience.svg'),
					unity: fs.readFileSync(DEST+'/assets/img/services/unity.svg'),
					efficiency: fs.readFileSync(DEST+'/assets/img/services/efficiency.svg'),
					time: fs.readFileSync(DEST+'/assets/img/services/time.svg')
				}
			},
			js: {
				loadjs: fs.readFileSync('node_modules/fg-loadjs/loadJS.js')
			}
		};

	// ---------------------------------------------------------------------------------------
	// PAGES
	// ---------------------------------------------------------------------------------------
	(function() {
		var stream = gulp.src('templates/pages/**/*.html')
			.pipe($.mustache(global_data, {
				extension: '.html'
			}))
			.pipe(gulp.dest('.tmp/html/rendered'));

		streams.add(stream);
	})();

	// ---------------------------------------------------------------------------------------
	// LEVELS
	// ---------------------------------------------------------------------------------------
	var levels = [];
	global_data.built_levels = [];

	fs.readdirSync('data/levels').forEach(level => {
		var data = require('./data/levels/'+ level);
		levels.push( data );
		levels = _.sortBy(levels, 'order');
	});

	levels.forEach(data => {
		var otherLevels = [];

		levels.forEach(level => {
			if ( level.slug != data.slug ) {
				otherLevels.push({
					slug: level.slug,
					icon: global_data.svg.levels[level.slug],
					title: level.title
				});
			}
		});

		var stream = gulp.src('templates/page-templates/level.html')
			.pipe($.mustache(Object.assign({},
				global_data, {
					self: Object.assign({},
						data, {
							other_levels: otherLevels
						}
					)
				}
			), {
				extension: '.html'
			}))
			.pipe($.rename(data.slug+'.html'))
			.pipe(gulp.dest('.tmp/html/rendered'));

		// pro vypsání v sitemap.xml
		global_data.built_levels.push(data.slug+'.html');

		streams.add(stream);
	});

	// ---------------------------------------------------------------------------------------
	// MISC
	// ---------------------------------------------------------------------------------------
	if ( ENV == 'dist' ) {
		(function() {
			// render sitemap.xml & robots.txt
			var miscStream = gulp.src([
					'sitemap.xml',
					'robots.txt'
				])
				.pipe($.mustache(global_data))
				.pipe(gulp.dest(DEST));

			streams.add(miscStream);
		}());
	}

	return streams.isEmpty() ? null : streams;
});

// ===============================================================
// HTML PROCESSING
// ===============================================================
// Run after CSS & JS are revisioned and revManifest created.
gulp.task('html', ['css', 'js', 'render_pages'], function() {
	var assetsRevisions = gulp.src(revManifestsBase+'/*.json');

	return gulp.src('.tmp/html/rendered/**/*.html')
		.pipe($.if(ENV == 'dist', $.revReplace({
			canonicalUris: false,
			replaceInExtensions: ['.html'],
			manifest: assetsRevisions
		})))
		.pipe($.if(ENV == 'dist', $.htmlmin({
			processConditionalComments: true,
			removeComments: true,
			removeCommentsFromCDATA: true,
			removeCDATASectionsFromCDATA: true,
			collapseWhitespace: true,
			conservativeCollapse: true,
			collapseInlineTagWhitespace: true,
			collapseBooleanAttributes: true,
			removeTagWhitespace: true,
			removeAttributeQuotes: true,
			removeRedundantAttributes: true,
			useShortDoctype: true,
			removeEmptyAttributes: true,
			removeScriptTypeAttributes: true,
			removeOptionalTags: true,
			decodeEntities: true,
			minifyJS: true,
			minifyCSS: true,
			quoteCharacter: '"'
		})))
		.pipe(gulp.dest(DEST))
		.pipe($.connect.reload());
});

// ===============================================================
// SERVER
// ===============================================================
// Run a local server at http://localhost:8000.
gulp.task('connect', function() {
	$.connect.server({
		root: DEST,
		port: 8000,
		livereload: true
	});
});

// ===============================================================
// BUILD
// ===============================================================
gulp.task('build', function() {
	// Wait for clean to finish before running anything else.
	runSequence('clean', ['css', 'js', 'img', 'copy', 'html']);
});

// ===============================================================
// SERVE
// ===============================================================
gulp.task('serve', ['build', 'connect']);

// ===============================================================
// DEVELOP
// ===============================================================
gulp.task('default', function() {
	ENV = 'dev';
	DEST = 'dev';

	// Wait for clean to finish before running anything else.
	runSequence('clean', ['css', 'js', 'img', 'copy', 'html'], 'connect');

	gulp.watch('assets/scss/**/*.scss', ['css']);
	gulp.watch('assets/js/**/*.js', ['js']);
	gulp.watch(['assets/img/**/*.{gif,jpg,png,svg}', '!assets/img/favicons/**/*'], ['img']);
	gulp.watch(['assets/font/**/*.{woff,woff2}', '!assets/font/original/**/*'], ['copy']);
	gulp.watch(['templates/**/*.html'], ['html']);
});
