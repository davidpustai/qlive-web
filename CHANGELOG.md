# Change Log

## [Unreleased]
### Added
- loga klientů na domovskou stránku


## [1.1.2] - 2017-10-12
### Changed
- vypuštění věty "Když už, tak už!"


## [1.1.1] - 2017-10-11
### Fixed
- centrování mapy


## [1.1.0] - 2017-10-07
### Added
- interaktivní mapa na stránku Kontakt

### Changed
- odebrán okraj tlačítka pro odeslání kontaktního formuláře
- úpravy textů na většině stránek
- vykání s velkým "V" na malé "v"

### Removed
- mapa stránek :disappointed:

### Fixed
- sjednoceny velikosti ikon


## [1.0.1] - 2017-09-10
### Changed
- sjednocení mezer napříč webem (úpravy na stránce Kontakt a na stránkách jednotlivých úrovní)

### Fixed
- odebrán stín inputů v Safari


## [0.8.0] - 2017-08-31
### Added
- mapa stránek

### Changed
- menší obrázky v záhlaví pro rychlejší načtení

### Fixed
- neprocházet 404 stránku roboty
- relativní -> absolutní odkazy na stránky
- chybějící nadpis první úrovně na domovksé stránce


## [0.7.0] - 2017-08-30
### Added
- sitemap.xml

### Changed
- menší ikony v sekcích na domovské stránce
- menší text a orámování odkazu v kontaktním pruhu, bílá barva, bohužel nepřístupná, ale odpovídá barvě v aplikaci klienta
- více prostoru nad a pod info sekcemi na stránkách úrovní

### Fixed
- zarovnání menu s okrajem stránky
- zarovnání okrajů na stránce Kontakt
- aktuální rok v proměnné `current_year`
- bílé pozadí inputů i ve FF
- přetékání záhlaví v IE
- zarovnání info sekcí v IE11
- načítání picturefill.js
- zarovnání menu v IE10


## [0.6.1] - 2017-08-23
### Fixed
- přebytečný text v emailu


## [0.6.0] - 2017-08-23
### Added
- picturefill
- obsah stránky Kontakt s funkčním formulářem

### Changed
- text a velikost fb odkazu v zápatí

### Removed
- sekce Projekty, Ohlasy a Klienti na domovské stránce

### Fixed
- zápis telefonních čísel pro čtečky


## [0.5.1] - 2017-08-21
### Fixed
- občas se bortící build


## [0.5.0] - 2017-08-21
### Added
- základ domovksé a kontaktní stránky
- při zobrazení celého menu se záhlaví přilepí při scrollování nahoru k okraji viewportu

### Changed
- oddělení kontaktu od úrovní automatizace v hlavním menu
- základní dimenze obrázků na 360px viewportu pro rychlejší vykreslení
- jemnější přechod pozadí info sekce
- zarovnání odkazu na kontaktní formulář v kontaktním pruhu na desktopu vedle textu
- menší text (a mezery) v info sekcích

### Removed
- text "MENU" z tlačítka pro rozbalení/sbalení menu
- rozlišení telefonního odkazu v zápatí
- debouncedresize jQuery event
- asynchronní načítání CSS kvůli závislosti JS

### Fixed
- chybějící obrázek v záhlaví stránky Interiéry
- překlep v nadpise Klimatizovaný domov


## [0.4.1] - 2017-08-17
### Added
- přechod z mobilního vzhledu menu zpět na desktopové při odpovídající velikosti viewportu

### Fixed
- font bez rozšířené znakové sady latinky


## [0.4.0] - 2017-08-17
### Changed
- menší text v kontaktní lince a tlačítkový vzhled okdazu
- při interakci se položka menu (a menu úrovní) nepodtrhne, ale změní barvu
- užší okraj menu a podtržení titulku stránky
- sytější bílý overlay pod titulkem stránky, ale pouze pod textem
- menší menu

### Removed
- Modernizr

### Fixed
- sjednocení zelené barvy
- zarovnání položek menu úrovní


## [0.3.0] - 2017-08-15
### Added
- GA & Smartlook tracking
- navigační zkratky (pohyb tabulátorem)
- další řezy písma - light, semibold, bold

### Changed
- přidán druhý telefon do obrázku v info sekci na stránce Automatizace
- menší menu
- lepší proporce fallback fontu
- menší maximální šířka hlavního obsahového sloupce

### Fixed
- stockové obrázky s vodotiskem
- reduced motion query pro přechody
- zarovnání sekcí v zápatí s krajem hlavního sloupce


## [0.2.0] - 2017-08-14
### Added
- favicony
- manifest.json
- stránky Osvětlení, Ozvučení, Zabezpečení, Klimatizace, Interiéry

### Changed
- použitelnější HTML title
- sjednocení mezer pod a nad nadpisem a obrázkem info sekce
- menší maximální velikost ikon dalších úrovní
- sjednocení prokladu kontaktních informací v zápatí
- menší logo v záhlaví při menším rozlišení
- zarovnání některých sekcí s okrajem hlavního sloupce


## [0.1.0] - 2017-07-20
### Added
- stránka Automatizace
- vlastní web font Poppins

### Changed
- podpora pouze posledních verzí prohlížečů a IE10
