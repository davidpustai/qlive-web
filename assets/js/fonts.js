(function() {
	var regular		= new FontFaceObserver('Poppins'),
		bold		= new FontFaceObserver('Poppins', { weight: '700'}),
		light		= new FontFaceObserver('Poppins', { weight: '300'}),
		semibold	= new FontFaceObserver('Poppins', { weight: '600'});

	Promise.all([
		regular.load(),
		bold.load(),
		light.load(),
		semibold.load()
	]).then(function() {
		document.documentElement.className += ' has-fonts-loaded';
	});

	// Optimization for Repeat Views
	sessionStorage.foutFontsLoaded = true;
})();
