function contactForm() {
	var formHTML =
		'<section class="contact-form">'+
			'<h2 class="contact-form-title">Dejte nám vědět víc o vašem projektu, konzultujte ho s&nbsp;námi!</h2>'+
			'<form>'+
				'<div class="contact-form-line-inputs">'+
					'<div class="contact-form-input">'+
						'<label for="contact-form-name">Jméno</label>'+
						'<input type="text" id="contact-form-name" name="name" required>'+
					'</div>'+
					'<div class="contact-form-input">'+
						'<label for="contact-form-tel">Telefonní číslo</label>'+
						'<input type="tel" id="contact-form-tel" name="tel" required>'+
					'</div>'+
					'<div class="contact-form-input">'+
						'<label for="contact-form-email">E-mail</label>'+
						'<input type="email" id="contact-form-email" name="email" required>'+
					'</div>'+
				'</div>'+
				'<div class="contact-form-input">'+
					'<label for="contact-form-text">Popište váš projekt</label>'+
					'<textarea id="contact-form-text" name="text" required></textarea>'+
				'</div>'+
				'<div class="contact-form-input">'+
					'<input type="submit" value="Odeslat">'+
				'</div>'+
			'</form>'+
		'</section>';

	function init() {
		var $placeholder = $('.js-contact-form');

		if ( $placeholder.length ) {
			var $form = createForm( $placeholder );
			ajaxifyForm( $form );
		}
	}

	function createForm( $placeholder ) {
		var $section = $(formHTML);

		$placeholder.replaceWith($section);

		return $section.find('form');
	}

	function ajaxifyForm($form) {
		$form.on('submit', function(e) {
			e.preventDefault();

			$.ajax({
				url: '/scripts/contact.php',
				type: 'POST',
				data: $form.serialize(),
				error: function(xhr, text, err) {
					message($form, 'Při odeslání došlo k chybě.');
					console.error(err);
				},
				success: function(res, text, xhr) {
					res = JSON.parse(res);

					switch (res.status) {
						case 'err': message($form, 'Při odeslání došlo k chybě.'); break;
						case 'mistake': message($form, res.data); break;
						case 'ok': $('.contact .fb-link').focus(); $form.replaceWith('<p class="contact-form-sent" role="alert">Vaše zpráva byla uspěšně odeslána, budeme vás kontaktovat.</p>'); break; // TODO: odkaz na fb hardcodovaný, lepší by bylo vyhledat předchozí focusovatelný prvek
					}
				}
			});
		});
	}

	function message($form, text) {
		var $alerts = $form.find('.contact-form-alert');

		if ( $alerts.length ) {
			$alerts.remove();
		}

		$alerts = $('<div class="contact-form-alert" role="alert"></div>');

		if ( typeof text === typeof "" ) {
			$alerts.append('<p>'+text+'</p>');
		}
		else if ( typeof text === typeof [] ) {
			for ( var i = 0; i < text.length; i ++ ) {
				$alerts.append('<p>'+text[i]+'</p>');
			};
		}

		$alerts.appendTo($form);
	}

	init();
}
