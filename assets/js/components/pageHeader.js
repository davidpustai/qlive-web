function pageHeader() {
	function init() {
		var $header = $('.page-header');

		if ( $header.length ) {
			var sticky = false;

			if ( 'matchMedia' in window ) {
				var bp = get_media_query('main-nav-inline');

				// desktop
				if ( matchMedia(bp).matches ) {
					stickOnScrollUp($header);
					sticky = true;
				}

				// reset/initialize mobile behaviour on window resize
				$(window).on('resize', _.debounce(function() {
					var matches = matchMedia(bp).matches;

					// desktop -> mobile
					if ( ! matches && sticky ) {
						stickOut($header);
						sticky = false;
					}
					// mobile -> desktop
					else if ( matches && ! sticky ) {
						stickOnScrollUp($header);
						sticky = true;
					}
				}, 150));
			}
		}
	}

	/**
	 * Přidá třídu záhlaví, pokud je focus uvnitř.
	 */
	function checkFocusInHeader() {
		var $header = $('.page-header');

		if ( $(document.activeElement).closest('.page-header').length ) {
			$header.addClass('has-focus-within');
		}
		else {
			$header.removeClass('has-focus-within');
		}
	}

	/**
	 * Při skrolování nahoru přilepíme menu k hornímu okraji viewportu.
	 * https://medium.com/@roberthigdon/medium-style-sticky-header-7ab232abea7f
	 */
	var lastToTop = 0,
		$header = $('.page-header');

	var hidingShowing = _.debounce(function() {
		var toTop = $('html, body').scrollTop();

		// scrolling down & header is not naturaly visible => hide it
		if ( toTop > lastToTop && toTop > $header.outerHeight() ) {
			$header.addClass('is-hidden');
		}
		else {
			$header.removeClass('is-hidden');
		}

		lastToTop = toTop;
	}, 16);

	/**
	 * Inicializace přilepování.
	 */
	function stickOnScrollUp( $header ) {
		$header.addClass('is-sticky');
		$(document)
			.on('scroll', hidingShowing)
			.on('focusin focusout', checkFocusInHeader);
	};

	/**
	 * Reset přilepování.
	 */
	function stickOut( $header ) {
		$header.removeClass('is-sticky');
		$(document)
			.off('scroll', hidingShowing)
			.off('focusin focusout', checkFocusInHeader);
	};

	init();
}
