function mainNav() {
	function init() {
		var $nav = $('.main-nav');

		if ( $nav.length ) {
			var hamburger = false;

			if ( 'matchMedia' in window ) {
				var bp = get_media_query('main-nav-inline');

				// mobile
				if ( ! matchMedia(bp).matches ) {
					makeHamburger($nav);
					hamburger = true;
				}

				// reset/initialize mobile behaviour on window resize
				$(window).on('resize', _.debounce(function() {
					var matches = matchMedia(bp).matches;

					// desktop -> mobile
					if ( ! matches && ! hamburger ) {
						makeHamburger($nav);
						hamburger = true;
					}
					// mobile -> desktop
					else if ( matches && hamburger ) {
						destroyHamburger($nav);
						hamburger = false;
					}
				}, 150));
			}
		}
	}

	function makeHamburger( $nav ) {
		var $button = $(
				'<button class="main-nav-toggle" type="button" aria-controls="hlavni-nabidka" aria-expanded="false" aria-labelledby="main-nav-label" title="'+$nav.find('#main-nav-label').text()+'">'+
					'<div class="main-nav-toggle-hamburger">'+
						'<span class="main-nav-toggle-hamburger-line"></span>'+
						'<span class="main-nav-toggle-hamburger-line"></span>'+
						'<span class="main-nav-toggle-hamburger-line"></span>'+
					'</div>'+
				'</button>'
			);

		// if focus is in nav, move it to button
		var moveFocus = false;
		if ( $(document.activeElement).closest('.main-nav').length ) {
			moveFocus = true;
		}

		$nav
			.attr('id', 'hlavni-nabidka')
			.attr('aria-hidden', 'true')
			.addClass('is-revealing');

		$('.page-header').addClass('has-main-nav-revealing')

		$button
			.appendTo( $('.page-header-top-bar') )
			.on('click', function() {
				if ( $button.attr('aria-expanded') == 'false' ) {
					$button.attr('aria-expanded', 'true');
					$nav.attr('aria-hidden', 'false');
				}
				else {
					$button.attr('aria-expanded', 'false');
					$nav.attr('aria-hidden', 'true');
				}
			});

		if ( moveFocus ) {
			$button.focus();
		}
	}

	function destroyHamburger( $nav ) {
		$nav
			.attr('aria-hidden', 'false')
			.removeClass('is-revealing');

		$('.page-header').removeClass('has-main-nav-revealing')

		var $toggle = $('.main-nav-toggle');

		// if focus is on toggle, move it to nav
		if ( $toggle.get(0) === document.activeElement ) {
			$nav.find('a').first().focus();
		}

		$toggle.remove();
	}

	init();
}
