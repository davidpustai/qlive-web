function a11ySkipTo() {
	function init() {
		var $links = $('.a11y-skip-to');

		if ( $links.length ) {
			moveFocusOnClick( $links );
		}
	}

	/**
	 * Na kliknutí na odkaz přesune focus na danou sekci.
	 */
	function moveFocusOnClick( $links ) {
		$links
			.each(function() {
				var $link = $(this),
					$target = $($link.attr('href'));

				// pokud je sekce již "focusovatelná" nebo již má nenulový tabindex nastavený, tabindex není třeba nsatavovat
				if ( ! ( ( typeof $target.attr('tabindex') !== typeof undefined && $target.attr('tabindex') != 0 ) || $target.filter('input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), a[href]') ) ) {
					$target.attr('tabindex', '-1');
				}

				$link.on('click', function() {
					setTimeout(function() {
						$target.focus();
					});
				});
			});
	};

	init();
}
