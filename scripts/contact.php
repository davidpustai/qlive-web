<?php

	function response($status, $data = null) {
		$json = array(
			"status" => $status
		);

		if ( $data != null ) {
			$json["data"] = $data;
		}

		echo json_encode($json);
		die;
	}

	function getIncommingData() {
		if ( ! isset($_POST["name"]) || ! isset($_POST["tel"]) || ! isset($_POST["email"]) || ! isset($_POST["text"]) ) {
			response("err");
		}

		$name = $_POST["name"];
		$tel = $_POST["tel"];
		$email = $_POST["email"];
		$text = $_POST["text"];

		$mistakes = array();

		if ( strlen($name) < 1 ) {
			$mistakes[] = "Vyplňte, prosím, vaše jméno.";
		}

		if ( strlen($tel) < 1 ) {
			$mistakes[] = "Vyplňte, prosím, vaše telefonní číslo.";
		}

		if ( strlen($email) < 1 ) {
			$mistakes[] = "Vyplňte, prosím, váš e-mail.";
		}

		if ( strlen($text) < 1 ) {
			$mistakes[] = "Popište, prosím, váš projekt.";
		}

		if ( count($mistakes) ) {
			response("mistake", $mistakes);
		}

		return array("name" => $name, "tel" => $tel, "email" => $email, "text" => $text);
	}

	function cleanEmailString($string) {
		$bad = array("content-type", "bcc:", "to:", "cc:", "href");
		return strip_tags( str_replace($bad, "", $string) );
	}

	function send($data) {
		$body = "Na vašich webových stránkách qlive.cz došlo k odeslání kontaktního formuláře.\n\n".

				"Jméno:\n".
				cleanEmailString($data["name"])."\n\n".

				"Telefonní číslo:\n".
				cleanEmailString($data["tel"])."\n\n".

				"E-mail:\n".
				cleanEmailString($data["email"])."\n\n".

				"Zpráva\n".
				nl2br( cleanEmailString($data["text"]) )."\n";

		if ( ! mail("napis@qlive.cz", "Nová zpráva z webu qlive.cz", $body, "From: napis@qlive.cz\r\n") ) {
			response("err");
		}
	};


	$data = getIncommingData();

	send($data);

	response("ok");

?>
